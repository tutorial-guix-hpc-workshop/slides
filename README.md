# Org mode and Guix tutorial - Guix HPC Workshop: Slides

[![pipeline status](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/slides/badges/main/pipeline.svg)](https://gitlab.inria.fr/tutorial-guix-hpc-workshop/slides/-/commits/main)

Introductory slides for the tutorial **How to use Org mode and Guix to build a
reproducible experimental study**.

[View slides](https://tutorial-guix-hpc-workshop.gitlabpages.inria.fr/slides/slides.pdf)

## Local build

This project uses [GNU Guix](https://guix.gnu.org) to manage its software
environment. Use the following command to reproduce the latter and build PDF
slides from their [Org mode](https://orgmode.org) source `slides.org`.

```bash
guix time-machine -C ./.guix/channels.scm -- \
     shell --pure -m ./.guix/manifest.scm -- \
     emacs --batch --no-init-file --load publish.el \
     --eval '(org-publish "slides")'
```
